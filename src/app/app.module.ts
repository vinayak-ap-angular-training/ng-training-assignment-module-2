import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';

/* Module Imports */
import { LoginModule } from './login/login.module';
import { Featuremodule1Module } from './featuremodule1/featuremodule1.module';
import { Featuremodule2Module } from './featuremodule2/featuremodule2.module';

@NgModule({
    declarations: [AppComponent],
    imports: [
        BrowserModule,
        LoginModule,
        Featuremodule1Module,
        Featuremodule2Module,
    ],
    providers: [],
    bootstrap: [AppComponent],
})
export class AppModule {}
